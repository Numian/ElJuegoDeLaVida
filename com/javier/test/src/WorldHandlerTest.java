package src;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Javier on 19/08/2016.
 */
public class WorldHandlerTest {

    @Test
    public void checkNumberOfLivingSurroundingCellsTest() {
        World world = new World(2, 2);
        world.getMap()[1][1].setAlive(true);

        int result00 = WorldHandler.checkNumberOfLivingSurroundingCells(world, 0, 0);
        int result01 = WorldHandler.checkNumberOfLivingSurroundingCells(world, 0, 1);
        int result10 = WorldHandler.checkNumberOfLivingSurroundingCells(world, 1, 0);
        int result11 = WorldHandler.checkNumberOfLivingSurroundingCells(world, 1, 1);
        WorldHandler.draw(world);

        Assert.assertTrue(result00 == 1);
        Assert.assertTrue(result01 == 1);
        Assert.assertTrue(result10 == 1);
        Assert.assertTrue(result11 == 0);
    }

    @Test
    public void isValidPositionTest() {
        World world = new World(2, 2);

        Assert.assertTrue(WorldHandler.isValidPosition(world, 0, 0));
        Assert.assertTrue(WorldHandler.isValidPosition(world, 1, 0));
        Assert.assertFalse(WorldHandler.isValidPosition(world, 3, 0));
        Assert.assertFalse(WorldHandler.isValidPosition(world, -1, -1));
    }

    @Test
    public void createCellWhenThereAreThreeOreMoreSurroundingCells() {
        World world = new World(2, 2);
        World nextWorld = new World(2, 2);
        world.getMap()[0][1].setAlive(true);
        world.getMap()[1][0].setAlive(true);
        world.getMap()[1][1].setAlive(true);
        int xPositionOfNewCell = 0;
        int yPositionOfNewCell = 0;

        WorldHandler.copyWorld(world, nextWorld);
        WorldHandler.createCell(world, nextWorld, xPositionOfNewCell, yPositionOfNewCell);
        WorldHandler.draw(world);
        WorldHandler.draw(nextWorld);

        Assert.assertTrue(nextWorld.getMap()[xPositionOfNewCell][yPositionOfNewCell].isAlive());
    }

    @Test
    public void dontCreateCellWhenThereAreLessThanThreeSurroundingCells() {
        World world = new World(2, 2);
        World nextWorld = new World(2, 2);
        world.getMap()[0][1].setAlive(true);
        world.getMap()[1][0].setAlive(true);
        int xPositionOfNewCell = 0;
        int yPositionOfNewCell = 0;

        WorldHandler.copyWorld(world, nextWorld);
        WorldHandler.createCell(world, nextWorld, xPositionOfNewCell, yPositionOfNewCell);
        WorldHandler.draw(world);
        WorldHandler.draw(nextWorld);

        Assert.assertTrue(!nextWorld.getMap()[xPositionOfNewCell][yPositionOfNewCell].isAlive());
    }

    @Test
    public void whenAliveKillWhenThereAreLessThanTwoSurroundingCells() {
        World world = new World(2, 2);
        World nextWorld = new World(2, 2);
        world.getMap()[0][0].setAlive(true);
        world.getMap()[1][0].setAlive(true);
        int xPositionOfNewCell = 1;
        int yPositionOfNewCell = 0;

        WorldHandler.copyWorld(world, nextWorld);
        WorldHandler.killCell(world, nextWorld, xPositionOfNewCell, yPositionOfNewCell);
        WorldHandler.draw(world);
        WorldHandler.draw(nextWorld);

        Assert.assertTrue(!nextWorld.getMap()[xPositionOfNewCell][yPositionOfNewCell].isAlive());
    }

    @Test
    public void whenAliveDoNothingWhenThereAreTowOreMoreSurroundingCells() {
        World world = new World(2, 2);
        World nextWorld = new World(2, 2);
        world.getMap()[0][0].setAlive(true);
        world.getMap()[1][0].setAlive(true);
        world.getMap()[1][1].setAlive(true);
        int xPositionOfNewCell = 1;
        int yPositionOfNewCell = 0;

        WorldHandler.copyWorld(world, nextWorld);
        WorldHandler.killCell(world, nextWorld, xPositionOfNewCell, yPositionOfNewCell);
        WorldHandler.draw(world);
        WorldHandler.draw(nextWorld);

        Assert.assertTrue(nextWorld.getMap()[xPositionOfNewCell][yPositionOfNewCell].isAlive());
    }
}