package src;

/**
 * Created by Javier on 19/08/2016.
 */
public class Cell {

    private boolean alive;

    public Cell(){
        alive = false;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
}
