package src;

import java.util.ArrayList;

/**
 * Created by Javier on 19/08/2016.
 */
public class Main {

    private static ArrayList<World> worldCollection;

    public static void main(String... args) {
        worldCollection = new ArrayList<>();
        int rows = 6;
        int columns = 6;
        int time = 0;
        generateWorld(rows, columns);
        for (int i = 0; i < rows; i++) {
            worldCollection.get(0).getMap()[i][0].setAlive(true);
        }

        while (true) {
            System.out.println("Número: " + time);
            WorldHandler.draw(worldCollection.get(time));
            createNextWorld(worldCollection.get(time), rows, columns);
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    WorldHandler.createCell(worldCollection.get(time), worldCollection.get(time + 1), i, j);
                    WorldHandler.killCell(worldCollection.get(time), worldCollection.get(time + 1), i, j);
                }
            }
            if (checkBothWorlds(worldCollection.get(time), worldCollection.get(time + 1), rows, columns)) {
                break;
            }
            time++;
        }
    }

    private static boolean checkBothWorlds(World world, World nextWorld, int rows, int columns) {
        int totalCoincidences = rows * columns;
        int coincidences = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (world.getMap()[i][j].isAlive() == nextWorld.getMap()[i][j].isAlive()) {
                    coincidences++;
                }
            }
        }
        if (totalCoincidences == coincidences) {
            return true;
        }else{
            return false;
        }
    }

    private static void generateWorld(int rows, int columns) {
        worldCollection.add(new World(rows, columns));
    }

    private static void createNextWorld(World world, int rows, int columns) {
        World nextWorld = new World(rows, columns);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (world.getMap()[i][j].isAlive()) {
                    nextWorld.getMap()[i][j].setAlive(true);
                }
            }
        }
        worldCollection.add(nextWorld);
    }
}
