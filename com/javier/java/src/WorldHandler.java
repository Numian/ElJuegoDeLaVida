package src;

/**
 * Created by Javier on 19/08/2016.
 */
public class WorldHandler {

    public static int checkNumberOfLivingSurroundingCells(World world, int xPosition, int yPosition) {
        int numberIfLivingSurroundingCells = 0;

        for (int i = xPosition - 1; i <= xPosition + 1; i++) {
            for (int j = yPosition - 1; j <= yPosition + 1; j++) {
                if (isValidPosition(world, i, j) && !(i == xPosition && j == yPosition) && world.getMap()[i][j].isAlive()) {
                    numberIfLivingSurroundingCells++;
                }
            }
        }
        return numberIfLivingSurroundingCells;
    }

    public static boolean isValidPosition(World world, int i, int j) {
        int rows = world.getRows();
        int columns = world.getColumns();
        if (i >= 0 && j >= 0 && i < rows && j < columns) {
            return true;
        } else {
            return false;
        }
    }

    public static void createCell(World world, World nextWorld, int xPosition, int yPosition) {
        if (checkNumberOfLivingSurroundingCells(world, xPosition, yPosition) > 2) {
            nextWorld.getMap()[xPosition][yPosition].setAlive(true);
        }
    }

    public static void killCell(World world, World nextWorld, int xPosition, int yPosition) {
        if (checkNumberOfLivingSurroundingCells(world, xPosition, yPosition) < 2) {
            nextWorld.getMap()[xPosition][yPosition].setAlive(false);
        }
    }

    public static void draw(World world) {
        int rows = world.getRows();
        int columns = world.getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (world.getMap()[i][j].isAlive()) {
                    System.out.printf("1");
//                    System.out.printf(i+""+j);
                } else {
                    System.out.printf("0");
//                    System.out.printf(i+""+j);
                }
            }
            System.out.println();
        }
    }

    public static void copyWorld(World world, World nextWorld) {
        for (int i = 0; i < world.getRows(); i++) {
            for (int j = 0; j < world.getColumns(); j++) {
                if (world.getMap()[i][j].isAlive()) {
                    nextWorld.getMap()[i][j].setAlive(true);
                } else {
                    nextWorld.getMap()[i][j].setAlive(false);
                }
            }
        }
    }
}
