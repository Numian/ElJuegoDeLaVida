package src;

import java.util.Map;

/**
 * Created by Javier on 19/08/2016.
 */
public class World {

    private int rows;
    private int columns;
    private Cell[][] map;

    public World(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        map = new Cell[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                map[i][j] = new Cell();
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public Cell[][] getMap() {
        return map;
    }
}
